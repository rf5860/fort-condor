#include "Warrior.h"
#include "./easylogging++.h"

Warrior::Warrior() {
  setSpeed(1);
  setRange(1);
  setStrength(30);
  setDefense(1);
  setCost(400);
  setHealth(200);
  spriteSheetName = "simple-fighter.png";
  spriteSheet = loadImage(spriteSheetName);
}

void Warrior::loadSprites() {
  loadSprite(&unitClips[0], 0);
  loadSprite(&unitClips[1], 1);
  loadSprite(&unitClips[2], 2);
  loadSprite(&unitClips[3], 3);
}
