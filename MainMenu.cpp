#include "MainMenu.h"
#include "Game.h"

int add() {return 0;}

MainMenu::MainMenu(int w, int h, SDL_Surface* s) : AbstractScreen(w, h, s) {
  LDEBUG << "Creating main menu.";
  option = 0;
  initialiseFonts();
  initialiseMenuOptions();
}

MainMenu::~MainMenu() {
  LDEBUG << "Destroying main menu";
  freeOptions();
  cleanupFonts();
  freeBackground();
 } 

void MainMenu::swapBoldMenuOptions(int original, int next) {
  free(menuOptions[original]);
  menuOptions[original] = TTF_RenderText_Solid(font, menuText[original], menuTextColor);
  free(menuOptions[next]);
  menuOptions[next] = TTF_RenderText_Solid(boldFont, menuText[next], menuTextColor);
}

void MainMenu::cleanupFonts() {
  LDEBUG << "Closing font";
  TTF_CloseFont(font);
  font = NULL;
  LDEBUG << "Closing boldFont";
  TTF_CloseFont(boldFont);
  boldFont = NULL;
}

void MainMenu::freeBackground() {
  LDEBUG << "Freeing background";
  SDL_FreeSurface(backgroundImage);
}

void MainMenu::freeOptions() {
  LDEBUG << "Freeing options";
  for (int i = 0; i < 3; i++) {
    SDL_FreeSurface(menuOptions[i]);
    menuOptions[i] = NULL;
  }
}

void MainMenu::renderMenuOptions() {
  for (int i = 0; i < 3; i++) {
    int xPosition = (screenWidth - menuOptions[i]->w) / 2;
    int yPosition = ((screenHeight + MENU_TEXT_HEIGHT * 1.5 * i) - MENU_TEXT_HEIGHT) / 2;
    applySurface(xPosition, yPosition,  menuOptions[i]);
  }
}

void MainMenu::applySurfaceAndRenderOptions() {
  applySurface(0, 0, backgroundImage);
  renderMenuOptions();
}

void MainMenu::initialiseMenuOptions() {
  LDEBUG << "Initialising menu options";
  for (int i = 0; i < 3; i++ ){
    TTF_Font* fontToUse = i == 0 ? boldFont : font;
    SDL_Surface* menuOption = TTF_RenderText_Solid(fontToUse, menuText[i], menuTextColor);
    menuOptions[i] = menuOption;
  }
}

void MainMenu::validateFont(TTF_Font* font) {
  // TODO: Implement validation and exceptions
  if (font == NULL) {
    LERROR << "Failed to load font.";
  }
}

void MainMenu::initialiseFonts() {
  LDEBUG << "Initialising fonts";
  menuTextColor.r = 255;
  menuTextColor.g = 255;
  menuTextColor.b = 255;
  menuText[NEW_GAME] = (char*)"New Game";
  menuText[LOAD_GAME] = (char*)"Load Game";
  menuText[OPTIONS] = (char*)"Options";
  font = TTF_OpenFont("consola.ttf", 28);
  boldFont = TTF_OpenFont("consola.ttf", 28);
  validateFont(font);
  validateFont(boldFont);
  TTF_SetFontStyle(boldFont, TTF_STYLE_BOLD);
}

void MainMenu::handleKeyEvent(SDL_Event* event) {
  int prevOption;
  if (event->type == SDL_KEYDOWN) {
    switch (event->key.keysym.sym) {
    case SDLK_UP:
      if (option > 0) {
	prevOption = option--;
	swapBoldMenuOptions(prevOption, option);
      }
      break;
    case SDLK_DOWN: 
      if (option < 2) {
	prevOption = option++;
	swapBoldMenuOptions(prevOption, option);

      }
      break;
    case SDLK_RETURN:
      handleMenuOptionSelection(option);
      break;
    default: break;
    }
  }
}

void MainMenu::startNewGame() {
  Game game(screenWidth, screenHeight, screen);
  game.run();
}

void MainMenu::displayOptionsMenu() {
}

void MainMenu::loadGame() {
}

void MainMenu::handleMenuOptionSelection(int option) {
  switch (option) {
  case NEW_GAME:
    startNewGame();
    break;
  case LOAD_GAME:
    loadGame();
    break;
  case OPTIONS:
    displayOptionsMenu();
    break;
  }
}

void MainMenu::loadBackgroundImage(std::string imageName) {
  backgroundImage = loadImage(imageName);
}
