#ifndef ABSTRACT_SCREEN_H
#define ABSTRACT_SCREEN_H

#include <SDL/SDL_ttf.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "./easylogging++.h"

class AbstractScreen {
 protected:
  int screenWidth, screenHeight;
  SDL_Surface* screen;
 public:
  AbstractScreen(int w, int h, SDL_Surface* s);
  SDL_Surface* loadImage(std::string name);
  void validateImage(SDL_Surface* image);
  void applySurface(int x, int y, SDL_Surface* source);
  void flipWithSuccess();
};
#endif
