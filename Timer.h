#ifndef GAME_TIMER_H
#define GAME_TIMER_H
#include <SDL/SDL.h>

class Timer {
 private:
  int startTicks, pausedTicks;
  bool paused, started;
 protected:
 public:
  Timer();
  void start();
  void stop();
  void pause();
  void unpause();
  int getTicks();
  bool isStarted();
  bool isPaused();
};
#endif
