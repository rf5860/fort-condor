#ifndef BASE_UNIT_H
#define BASE_UNIT_H
#include <SDL/SDL.h>
#include <string>

class BaseUnit {
 private:
  int xPosition, yPosition, speed, range, strength, defense, cost, health, currentFrame;
 protected:
  SDL_Rect unitClips[4];
  SDL_Surface* loadImage(std::string name);
  const char* spriteSheetName;
  SDL_Surface* spriteSheet;
 public:
  int getCurrentFrame();
  int getSpeed();
  int getRange();
  int getStrength();
  int getDefense();
  int getCost();
  int getHealth();
  int getWidth();
  int getHeight();
  int getX();
  int getY();
  void setCurrentFrame(int currentFrame);
  void setSpeed(int speed);
  void setRange(int range);
  void setStrength(int strength);
  void setDefense(int defense);
  void setCost(int cost);
  void setHealth(int health);
  void setX(int x);
  void setY(int y);
  virtual void loadSprites() = 0;
  void show(SDL_Surface* destination, int screenHeight);
  void loadSprite(SDL_Rect* rect, int widthMultiplier);
  BaseUnit();
  ~BaseUnit();
};

#endif
