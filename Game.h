#ifndef GAME_H
#define GAME_H
#include <SDL/SDL_ttf.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "AbstractScreen.h"
#include "BaseUnit.h"
#include <vector>

class Game : public AbstractScreen {
 private:
  float xScrollDelta, yScrollDelta;
  int currentX, currentY;
  std::vector<BaseUnit*> units;
 protected:
  SDL_Surface* mapImage;
 public:
  Game(int w, int h, SDL_Surface* screen);
  ~Game();
  void run();
  void renderUnits();
  void saveState();
  void loadState();
  void initialiseMap();
  void handleEvent(SDL_Event event);
  void handleKeyPress(SDL_Event event);
  void handleMouseMovement(SDL_Event event);
  float calculateScrollDelta(int currentPosition, int upperBound);
};
#endif
