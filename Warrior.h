#ifndef WARRIOR_H
#define WARRIOR_H

#include "BaseUnit.h"

class Warrior : public BaseUnit {
 private:
 protected:
 public:
  void loadSprites();
  Warrior();
};

#endif
