#ifndef MAIN_MENU_H
#define MAIN_MENU_H

#include <SDL/SDL_ttf.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <string>
#include "./easylogging++.h"
#include "AbstractScreen.h"

class MainMenu : public AbstractScreen {
 private:
  int option;
  SDL_Surface* menuOptions[3];
  SDL_Surface* backgroundImage;
  TTF_Font* font;
  TTF_Font* boldFont;
  SDL_Color menuTextColor;
 protected:
  void validateBackground();
  void validateFont(TTF_Font* font);
  void handleMenuOptionSelection(int option);
  void initialiseMenuOptions();
  void initialiseFonts();
  void cleanupFonts();
  void freeOptions();
  void freeBackground();
  void displayOptionsMenu();
  void swapBoldMenuOptions(int original, int next);
  void startNewGame();
  void loadGame();
  char* menuText[3];
  static const int MENU_TEXT_HEIGHT = 28;
 public:
  enum menu_options {
    NEW_GAME,
    LOAD_GAME,
    OPTIONS
  };
  MainMenu(int screenWidth, int screenHeight, SDL_Surface* screen);
  ~MainMenu();
  void applySurfaceAndRenderOptions();
  void renderMenuOptions();
  void handleKeyEvent(SDL_Event* event);
  void loadBackgroundImage(std::string imageName);
};
#endif
