#include "AbstractScreen.h"

AbstractScreen::AbstractScreen(int w, int h, SDL_Surface* s) {
  LDEBUG << "Creating abstract screen";
  screenWidth = w;
  screenHeight = h;
  screen = s;
}

SDL_Surface* AbstractScreen::loadImage(std::string name) {
  SDL_Surface* optimisedImage = NULL;
  LDEBUG << "Loading image " << name;
  SDL_Surface* loadedImage = IMG_Load(name.c_str());
  if (loadedImage != NULL) {
    LDEBUG << "Loading optimized image " << name;
    optimisedImage = SDL_DisplayFormat(loadedImage);
    SDL_FreeSurface(loadedImage);
  }

  validateImage(optimisedImage);
  return optimisedImage;
}

void AbstractScreen::validateImage(SDL_Surface* image) {
  // TODO: Implement validation and exceptions
  if (image == NULL) {
    LERROR << "Failed to load background image.";
  }
}

void AbstractScreen::applySurface(int x, int y, SDL_Surface* source) {
  SDL_Rect offset;
  offset.x = x;
  offset.y = y;
  SDL_BlitSurface(source, NULL, screen, &offset);
}

void AbstractScreen::flipWithSuccess() {
  if (SDL_Flip(screen) == -1) {
    // TODO: Implement exception.
  }
}
