#include "MainMenu.h"

_INITIALIZE_EASYLOGGINGPP

#define FLIP_SCREEN_WITH_SUCCESS(SCREEN)  if (SDL_Flip(SCREEN) == -1) {	\
    return;\
  }

const int SCREEN_WIDTH = 512;
const int SCREEN_HEIGHT = 512;
const int SCREEN_BPP = 32;

void configureLogging() {
  easyloggingpp::Configurations conf;
  conf.setToDefault();
  conf.parseFromText("*ALL:\nFORMAT = %level: %log");
}

bool init(SDL_Surface** screen) {
  configureLogging();
  if (SDL_Init( SDL_INIT_EVERYTHING) == -1) {
    return false;
  }

  *screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_SWSURFACE);
  if (screen == NULL) {
    return false;
  }
  if (TTF_Init() == -1) {
    return false;
  }

  SDL_WM_SetCaption("Fort Condor 0.1", NULL);

  return true;  
}

void cleanUp() {
  LDEBUG << "Quiting SDL";
  SDL_Quit();
  LDEBUG << "Quit SDL";
  LDEBUG << "Quiting TTF";
  TTF_Quit();
  LDEBUG << "Quit TTF";
}

void fillRectangle(SDL_Surface* screen, int x, int y, int w, int h, int color) {
  SDL_Rect rect = {x,y,w,h};
  SDL_FillRect(screen, &rect, color);
}


void mainLoop(SDL_Surface* screen) {
  bool quit = false;

  MainMenu mainMenu(SCREEN_WIDTH, SCREEN_HEIGHT, screen);
  mainMenu.loadBackgroundImage("FortCondor.png");
  mainMenu.applySurfaceAndRenderOptions();
  FLIP_SCREEN_WITH_SUCCESS(screen);
  SDL_Event event;
  while(!quit) {
    while (SDL_PollEvent(&event)) {
      if (event.type == SDL_KEYDOWN) {
	mainMenu.handleKeyEvent(&event);
	mainMenu.applySurfaceAndRenderOptions();
	FLIP_SCREEN_WITH_SUCCESS(screen);
      }
      quit = event.type == SDL_QUIT;
    }
  }
  LDEBUG << "Quitting";
}

int main(int argc, char **argv) {
  SDL_Surface* screen = NULL;

  if(!init(&screen)) {
    LERROR << "Faild initialisation";
    return 1;
  }

  mainLoop(screen);
  cleanUp();

  return 0;
}
