#include "BaseUnit.h"
#include "./easylogging++.h"
#include <SDL/SDL_image.h>

BaseUnit::BaseUnit(){
  xPosition = yPosition;
  setCurrentFrame(0);
}

BaseUnit::~BaseUnit(){
}

int BaseUnit::getCurrentFrame() {
  return currentFrame;
}

int BaseUnit::getSpeed() {
  return speed;
}

int BaseUnit::getRange() {
  return range;
}

int BaseUnit::getStrength() {
  return strength;
}

int BaseUnit::getDefense() {
  return defense;
}

int BaseUnit::getCost() {
  return cost;
}

int BaseUnit::getHealth() {
  return health;
}

int BaseUnit::getWidth() {
  return 25;
}

int BaseUnit::getHeight() {
  return 52;
}

int BaseUnit::getX() {
  return xPosition;
}

int BaseUnit::getY() {
  return yPosition;
}

void BaseUnit::loadSprite(SDL_Rect* rect, int widthMultiplier) {
  rect->x = getWidth() * widthMultiplier;
  rect->y = 0;
  rect->w = getWidth();
  rect->h = getHeight();
}

void BaseUnit::setCurrentFrame(int currentFrame) {
  this->currentFrame = currentFrame;
}

void BaseUnit::setSpeed(int speed) {
  this->speed = speed;
}

void BaseUnit::setRange(int range) {
  this->range = range;
}

void BaseUnit::setStrength(int strength){
  this->strength = strength;
}

void BaseUnit::setDefense(int defense){
  this->defense = defense;
}

void BaseUnit::setCost(int cost) {
  this->cost = cost;
}

void BaseUnit::setHealth(int health) {
  this->health = health;
}

void BaseUnit::setX(int x) {
  this->xPosition = x;
}

void BaseUnit::setY(int y) {
  this->yPosition = y;
}

void BaseUnit::show(SDL_Surface* destination, int screenHeight) {
  LDEBUG << "Rendering unit";
  SDL_Rect offset;
  offset.x = screenHeight - getHeight();
  offset.y = getY();
  SDL_BlitSurface(spriteSheet, &unitClips[currentFrame], destination, &offset);
}

SDL_Surface* BaseUnit::loadImage(std::string name) {
  SDL_Surface* optimisedImage = NULL;
  SDL_Surface* loadedImage = IMG_Load(name.c_str());
  if (loadedImage != NULL) {
    optimisedImage = SDL_DisplayFormat(loadedImage);
    SDL_FreeSurface(loadedImage);
  }

  return optimisedImage;
}
