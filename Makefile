CC=g++
LIBS=-lmingw32 -lSDLmain -lSDL -lSDL_image -lSDL_ttf -lstdc++
SRC=fort-condor.cpp MainMenu.cpp MainMenu.h Game.cpp Game.h AbstractScreen.cpp AbstractScreen.h Timer.cpp Timer.h BaseUnit.cpp BaseUnit.h Warrior.cpp Warrior.h

all: fort-condor 

fort-condor: fort-condor.cpp MainMenu.cpp Game.cpp AbstractScreen.cpp Timer.cpp BaseUnit.cpp Warrior.cpp
	$(CC) -o fort-condor.exe $(SRC) $(LIBS)

.PHONY: clean
clean:
	rm -rf *.exe

.PHONY: run
run:
	fort-condor.exe

.PHONY: check-syntax
check-syntax:
	$(CC) -Wall -Wextra -fsyntax-only $(SRC) $(LIBS)
