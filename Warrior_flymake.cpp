#include "Warrior.cpp"

Warrior::Warrior() {
  setSpeed(1);
  setRange(1);
  setStrength(30);
  setDefense(1);
  setCost(400);
  setHealth(200);
}
