#include "Game.h"
#include "./easylogging++.h"
#include "Timer.h"
#include "Warrior.h"
#include <string>
#include <algorithm>
#include <vector>

const char* mapName = "Map1.png";
const int FRAMES_PER_SECOND = 60;
const float SCROLL_AT_UPPER_BOUNDARY = 0.9;
const float SCROLL_AT_LOWER_BOUNDARY = 0.1;

Game::Game(int w, int h, SDL_Surface* s) : AbstractScreen(w, h, s){
  LDEBUG << "Creating game";
  initialiseMap();
  xScrollDelta = yScrollDelta = currentX = currentY = 0;
  Warrior* warr = new Warrior();
  warr->loadSprites();
  units.push_back(warr);
}

Game::~Game(){
}

void Game::initialiseMap() {
  mapImage = loadImage(mapName);
}

void Game::handleEvent(SDL_Event event) {
  switch(event.type) {
  case SDL_MOUSEMOTION:
    handleMouseMovement(event);
    break;
  case SDL_KEYDOWN:
    handleKeyPress(event);
    break;
  }
}

void Game::run() {
  Timer timer;
  boolean quit = false;
  SDL_Event event;
  while (!quit) {
    while (SDL_PollEvent(&event)) {
      handleEvent(event);
      quit = event.type == SDL_QUIT;
    }
    if( timer.getTicks() < 1000 / FRAMES_PER_SECOND ) {
      SDL_Delay(( 1000 / FRAMES_PER_SECOND ) - timer.getTicks());
    }
    currentX += xScrollDelta;
    currentX = currentX < -screenWidth ? -screenWidth : currentX;
    currentX = currentX > 0 ? 0 : currentX;
    currentY += yScrollDelta;
    currentY = currentY < -screenHeight ? -screenHeight : currentY;
    currentY = currentY > 0 ? 0 : currentY;
    applySurface(currentX, currentY, mapImage);
    renderUnits();
    flipWithSuccess();
  }
}

void Game::renderUnits() {
  for (std::vector<BaseUnit*>::iterator it = units.begin(); it != units.end(); it++) {
    (*it)->show(screen, screenHeight);
  }
}

void Game::saveState() {
}

void Game::loadState() {
}

void Game::handleKeyPress(SDL_Event event) {
  switch (event.key.keysym.sym) {
  default: break;
  }
}

float Game::calculateScrollDelta(int currentPosition, int upperBound) {
  float delta = 0.0;
  if (currentPosition > (SCROLL_AT_UPPER_BOUNDARY * upperBound)) {
    delta = -((0.5 / FRAMES_PER_SECOND) * upperBound);
  } else if (currentPosition < (SCROLL_AT_LOWER_BOUNDARY * upperBound)) {
    delta = ((0.5 / FRAMES_PER_SECOND) * upperBound);
  }

  return delta;
}

void Game::handleMouseMovement(SDL_Event event) {
  xScrollDelta = calculateScrollDelta(event.motion.x, screenWidth);
  yScrollDelta = calculateScrollDelta(event.motion.y, screenHeight);
}
